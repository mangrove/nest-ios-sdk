Pod::Spec.new do |s|

  s.name             = 'NestSDK'
  s.version          = '0.1.8'
  s.summary          = 'SDK for the Nest API on iOS [Unofficial]'

  s.description      = <<-DESC
                       SDK for the Nest API on iOS [Unofficial]

                       This open-source library allows you to integrate Nest API into your iOS app.

                       Learn more about Nest API at https://developer.nest.com/documentation/cloud/get-started
                       DESC

  s.homepage         = 'https://bitbucket.org/mangrove/nest-ios-sdk'
  s.license          = 'MIT'
  s.author           = { 'petroakzhygitov' => 'petro.akzhygitov@gmail.com' }

  s.platform     = :ios, '9.0'
  s.ios.deployment_target = '8.0'

  s.source           = { :git => 'https://bitbucket.org/mangrove/nest-ios-sdk.git', :branch => 'master' }

  s.requires_arc = true

  s.source_files = 'NestSDK/NestSDK/**/*.{h,m}'
  s.public_header_files = 'NestSDK/NestSDK/*.{h}'

  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/Database'
  s.dependency 'Firebase/Auth'
  s.dependency 'JSONModel'
  s.dependency 'SAMKeychain'

  s.frameworks = 'Security', 'CFNetwork', 'SystemConfiguration'
  s.libraries = 'c++', 'icucore'

  s.pod_target_xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '${PODS_ROOT}/Firebase', 'ENABLE_BITCODE' => 'NO', 'OTHER_LDFLAGS' => '-ObjC' }

end
