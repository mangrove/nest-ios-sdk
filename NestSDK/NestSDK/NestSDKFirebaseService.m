// Copyright (c) 2016 Petro Akzhygitov <petro.akzhygitov@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

@import Firebase;
@import FirebaseAuth;
#import "NestSDKFirebaseService.h"
#import "NestSDKAccessToken.h"
#import "NestSDKLogger.h"
#import "NestSDKError.h"


#pragma mark const
static NSString *const kArgumentNameAccessToken = @"accessToken";


@interface NestSDKFirebaseService ()

@property(nonatomic) NSMutableDictionary *handleToURLDictionary;

@end


@implementation NestSDKFirebaseService
#pragma mark Initializer

- (instancetype)initWithFirebase:(FIRDatabaseReference *)firebase {
    self = [super init];
    if (self) {
        _firebase = firebase;

        self.handleToURLDictionary = [[NSMutableDictionary alloc] init];
    }

    return self;
}

#pragma mark Private

- (void)_completeAuthenticationWithBlock:(NestSDKAuthenticableServiceCompletionBlock)block error:(NSError *)error {
    if (!block) return;

    block(error);
}

- (FIRDatabaseReference *)_firebaseWithURL:(NSString *)url {
    if (url.length == 0) return nil;
    return [self.firebase child:url];
}

#pragma mark Public

- (void)authenticateWithAccessToken:(NestSDKAccessToken *)accessToken
                    completionBlock:(NestSDKAuthenticableServiceCompletionBlock)completionBlock {
    // WARNING: Do not call unathenticate method while making re-authentication

    if (!accessToken) {
        NSError *error = [NestSDKError argumentRequiredErrorWithName:kArgumentNameAccessToken message:nil];
        [self _completeAuthenticationWithBlock:completionBlock error:error];

        return;
    }

    [NestSDKLogger logInfo:@"Authenticating..." from:self];

    __weak typeof(self) weakSelf = self;
    
    [[FIRAuth auth] signInWithCustomToken:accessToken.tokenString
                               completion:^(FIRUser *_Nullable user,
                                            NSError *_Nullable error) {
        typeof(self) self = weakSelf;
        if (!self) return;

        if (error) {
            [self _completeAuthenticationWithBlock:completionBlock error:error];

            return;
        }

        [self _completeAuthenticationWithBlock:completionBlock error:error];
    }];
}

- (void)unauthenticate {
    [self removeAllObservers];

    [[FIRAuth auth] signOut:nil];

    [NestSDKLogger logInfo:@"Unauthenticated!" from:self];
}

- (void)valuesForURL:(NSString *)url withBlock:(NestSDKServiceUpdateBlock)block {
    if (!block) return;

    //Firebase *firebase = [self _firebaseWithURL:url];
    [self.firebase observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        block(snapshot.value, nil);
    }
    withCancelBlock:^(NSError *error) {
        block(nil, error);
    }];
}

- (void)setValues:(NSDictionary *)values forURL:(NSString *)url withBlock:(NestSDKServiceUpdateBlock)block {
    //Firebase *firebase = [self _firebaseWithURL:url];

    // IMPORTANT to set withLocalEvents to NO.
    // More information here: https://www.firebase.com/docs/transactions.html
    [self.firebase runTransactionBlock:^FIRTransactionResult *(FIRMutableData *currentData) {
        [currentData setValue:values];

        return [FIRTransactionResult successWithValue:currentData];

    }          andCompletionBlock:^(NSError *error, BOOL committed, FIRDataSnapshot *snapshot) {
        if (block) block(snapshot.value, error);

    }             withLocalEvents:NO];
}

- (NestSDKObserverHandle)observeValuesForURL:(NSString *)url withBlock:(NestSDKServiceUpdateBlock)block {
    if (!block) return 0;

    FIRDatabaseReference *rootRef= [[FIRDatabase database] reference];
    
    FIRDatabaseHandle handle = [rootRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        block(snapshot.value, nil);
    }
    withCancelBlock:^(NSError *error) {
        block(nil, error);
    }];

    // Map handle and url, since to remove observer we will need same instance of firebase to call removeObserver
    self.handleToURLDictionary[@(handle)] = url;

    return handle;
}

- (void)removeObserverWithHandle:(NestSDKObserverHandle)handle {
    FIRDatabaseReference *rootRef= [[FIRDatabase database] reference];
    [rootRef removeObserverWithHandle:handle];
}

- (void)removeAllObservers {
    for (NSNumber *handle in self.handleToURLDictionary.allKeys) {
        [self removeObserverWithHandle:handle.unsignedIntegerValue];
    }

    [self.handleToURLDictionary removeAllObjects];
}

@end
